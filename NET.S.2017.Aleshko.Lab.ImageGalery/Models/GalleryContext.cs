﻿using System.Data.Entity;

namespace NET.S._2017.Aleshko.Lab.ImageGalery.Models
{
    public class GalleryContext: DbContext
    {
        public GalleryContext()
            : base("name=GalleryContext")
        {
            Database.SetInitializer(new DbInitializer());
        }

        public DbSet<ImageModel> Images { get; set; }
    }
}