﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;

namespace NET.S._2017.Aleshko.Lab.ImageGalery.Models
{
    public class DbInitializer: CreateDatabaseIfNotExists<GalleryContext>
    {
        protected override void Seed(GalleryContext context)
        {
            String[] imageNames = Directory.GetFiles("E:\\Lera\\VSProjects\\NET.S.2017.Aleshko.Lab.ImageGalery\\NET.S.2017.Aleshko.Lab.ImageGalery\\Content\\Images");
            
            foreach (string imageName in imageNames)
            {
                var imageModel = new ImageModel
                {
                    Author = "Author",
                    Comment = "Some text",
                    Image = File.ReadAllBytes(imageName),
                    SmallImage = ToSmallImage(new Bitmap(imageName), 250, 250),
                    Type = "image/" + imageName.Split('.').Last()
                };

                context.Images.Add(imageModel);
            }
        }

        private byte[] ToSmallImage(Bitmap image, int width, int height)
        {
            var smallImage = new Bitmap(image, new Size(width, height));
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(smallImage, typeof(byte[]));
        }

    }
}