﻿namespace NET.S._2017.Aleshko.Lab.ImageGalery.Models
{
    public class ImageModel
    {
        public int Id { get; set; }

        public byte[] Image { get; set; }

        public byte[] SmallImage { get; set; }

        public string Type { get; set; }

        public string Author { get; set; }

        public string Comment { get; set; }
    }
}