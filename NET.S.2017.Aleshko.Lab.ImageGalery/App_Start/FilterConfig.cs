﻿using System.Web;
using System.Web.Mvc;

namespace NET.S._2017.Aleshko.Lab.ImageGalery
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
