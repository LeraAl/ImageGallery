﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using NET.S._2017.Aleshko.Lab.ImageGalery.Models;

namespace NET.S._2017.Aleshko.Lab.ImageGalery.Controllers
{
    public class HomeController : Controller
    {
        private static readonly GalleryContext _db = new GalleryContext();

        public ActionResult Index()
        {
            var images = _db.Images;
            
            ViewData["Images"] = images;

            return View();
        }

        public FileResult GetSmallImage(int? id)
        {
            if (id == null) return null;

            ImageModel image = _db.Images.FirstOrDefault(img => img.Id == id);
            return File(image?.SmallImage, image?.Type);
        }

        public FileResult GetFullImage(int? id)
        {
            if (id == null) return null;

            ImageModel image = _db.Images.FirstOrDefault(img => img.Id == id);
            return File(image?.Image, image?.Type);
        }

    }
}